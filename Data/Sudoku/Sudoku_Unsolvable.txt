# A valid but unsolvable Sudoku puzzle.
# This puzzle is modified from Sudoku00.txt by changing the first 1 to a 2 and
# removing the 2s in the first row and the first column so the puzzle is valid.
# This puzzle is useful for quickly testing that the recursive solver fails.
2 0 3 4 5 6 7 8 9 
4 5 6 7 8 9 1 2 3 
7 8 9 1 2 3 4 5 6 
0 1 4 3 6 5 8 9 7 
3 6 5 8 9 7 2 1 4 
8 9 7 2 1 4 3 6 5 
5 3 1 6 4 2 9 7 8 
6 4 2 9 7 8 5 3 1 
9 7 8 5 3 1 6 4 2 
